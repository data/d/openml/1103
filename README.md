# OpenML dataset: yeast_gene

https://www.openml.org/d/1103

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Systematic determination of genetic network architecture.

Nature Genetics, 1999 Jul;22(3):281-5.
Data also used in Biclustering of Expression Data, by Yizong Cheng and George M. Church (web supplement)

S. Tavazoie, J.D. Hughes, M.J. Campbell, R.J. Cho, G.M. Church.
Dataset (17 instances x 1884 genes) in ARFF format, no labelled classes

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1103) of an [OpenML dataset](https://www.openml.org/d/1103). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1103/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1103/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1103/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

